# Create a global static IP address:

`gcloud compute addresses create <STATIC_IP_NAME> --global`

# Use in Ingress:

`
kind: Ingress
metadata:
  name: nginx
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "<STATIC_IP_NAME>"
spec:
  backend:
    serviceName: nginx
    servicePort: 80
`
